provider "yandex" {
  cloud_id  = var.yc_id
  token     = var.yc_token
  folder_id = var.yc_folder_id
  zone      = var.yc_zone
}

resource "yandex_vpc_network" "net0" {
  name = "net0"
}

resource "yandex_vpc_subnet" "subnet" {
  count        = 3
  network_id     = yandex_vpc_network.net0.id
  zone           = var.yc_zone
  name         = "subnet0-${count.index}"
  v4_cidr_blocks = [
    "10.${count.index + 130}.0.0/24"
  ]
}

data "yandex_compute_image" "ubuntu" {
  family = "ubuntu-2004-lts"
}

resource "yandex_compute_instance" "vm" {
  name        = "${"test0"}-${count.index}"
  count       = 3
  allow_stopping_for_update = true
  platform_id               = "standard-v3"
  zone                      = var.yc_zone

  resources {
    cores  = "2"
    memory = "2"
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu.id
      type     = "network-hdd"
      size     = 10
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet[count.index].id
    nat       = true
  }

  metadata = {
     ssh-keys = "ubuntu:${file("id_rsa.pub")}"
  }

  scheduling_policy {
    preemptible = true
  }
}
