terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

    backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     =  "asuhodola-ie7h"
    region     = "ru-central1-a"
    key        = "main-infra/terraform.tfstate"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}
